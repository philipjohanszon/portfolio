const port = 4999;
const express = require("express");
const app = express();

app.use(express.static(__dirname + '/public'));
app.use(express.urlencoded());
app.use(express.json());

app.get("/", (req, res) => {
    res.sendFile("./index.html", { root: __dirname });
});

app.listen(port);
