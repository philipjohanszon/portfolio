const renderDiv = document.getElementById("front");

function setup() {
    sketchWidth = renderDiv.clientWidth;
    sketchHeight = renderDiv.clientHeight;
    console.log(sketchHeight)
    let myCanvas = createCanvas(sketchWidth, sketchHeight);
    myCanvas.parent("front");

    background(0);
    createStars(100);
}

function draw() {

}

function createStars(amount) {
    for (let i = 0; i < amount; i++) {
        const xPos = random(10, sketchWidth - 10);
        const yPos = random(10, sketchHeight - 10);
        const radius1 = random(3, 4);
        const radius2 = random(5, 8);

        star(xPos, yPos, radius1, radius2, 5);
    }
}

function star(x, y, radius1, radius2, npoints) {
    let angle = TWO_PI / npoints;
    let halfAngle = angle / 2.0;
    beginShape();
    for (let a = 0; a < TWO_PI; a += angle) {
        let sx = x + cos(a) * radius2;
        let sy = y + sin(a) * radius2;
        vertex(sx, sy);
        sx = x + cos(a + halfAngle) * radius1;
        sy = y + sin(a + halfAngle) * radius1;
        vertex(sx, sy);
    }
    endShape(CLOSE);
}
